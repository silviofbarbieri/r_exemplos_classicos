# R_Exemplos_Clássicos



## Início

Exemplos clássicos de R são datasets comumente encontrados em ciência de dados como base de aprendizado e estudo de estatística. Eles cobrem desde análises descritivas básicas, passando pela produção de gráficos, testes de hipótese, até amostragem.


