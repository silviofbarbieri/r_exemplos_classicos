
setwd("C:\\Projetos\\Unesp\\BIOestat�stica\\Aula8")

# LIMPANDO AREA DE TRABALO

rm(list = ls())

# ATIVANDO PACOTE PARA LEITURA DE BANCO DE DADOS EM FORMATO xlsx

library(openxlsx)

# Importando banco de dados "bebes.xlsx"

bebes <- read.xlsx("bebes.xlsx")

# Verificando estrutura do banco de dados

str(bebes)
head(bebes)

# CRIANDO VARIAVEL PESO AO NASCER EM GRAMAS A PARTIR DA VARIAVEL PESO AO NASCER MEDIDO EM ONCAS

bebes$peso_nasc_g <- bebes$peso_nasc_oncas * 28.35


# CRIANDO VARIAVEL TEMPO DE GESTACAO MEDIDA EM SEMANAS

bebes$tempo_gestacao_semanas <- bebes$tempo_gestacao / 7


# CORRIGINDO AS VARIAVEIS PESO DOS PAIS ANTES DA GESTACAO DE MODO A CLASSIFICAR VALORES 99 COMO CORRESPONDENTO A DADOS AUSENTE (NA)


bebes$altura_mae_polegadas[bebes$altura_mae_polegadas == 99] <- NA

bebes$altura_pai_polegadas[bebes$altura_pai_polegadas == 99] <- NA


# CRIANDO VARIAVEIS COM PESO DOS PAIS EM KG, ALTURA EM METROS E INDICE DE MASSA CORPORAL 

bebes$peso_mae_kg_antes <- bebes$peso_mae_libras_antes / 2.205
bebes$peso_pai_kg_antes <- bebes$peso_pai_libras_antes / 2.205

bebes$altura_mae_m <- bebes$altura_mae_polegadas / 39.37
bebes$altura_pai_m <- bebes$altura_pai_polegadas / 39.37

bebes$imc_mae <- bebes$peso_mae_kg_antes / (bebes$altura_mae_m ^2)
bebes$imc_pai <- bebes$peso_pai_kg_antes / (bebes$altura_pai_m ^2)


# CRIANDO UMA VARIAVEL CATEGORICA PARA O INDICE DE MASSA CORPORAL DE CADA UM DOS PAIS DIVIDIDO NAS SEGUINTES CATEGORIAS
# <18.5, 18.6 A 24.9, 25 A 29.9, 30 OU MAIS

bebes$imc_mae_cat <- cut(bebes$imc_mae, breaks = c(-Inf, 18.5, 25, 30, Inf), right = F, labels = c("<18.5", "18.5 a 24.9", "25 a 29.9", ">=30 "))
summary(bebes$imc_mae_cat)

bebes$imc_pai_cat <- cut(bebes$imc_pai, breaks = c(-Inf, 18.5, 25, 30, Inf), right = F, labels = c("<18.5", "18.5 a 24.9", "25 a 29.9", ">=30 "))
summary(bebes$imc_pai_cat)

# REORDENANDO VARIAVEL NIVEL EDUCACIONAL MATERNO

bebes$educacao_mae <- factor(bebes$educacao_mae, levels = c("Menos de 8 anos",
                                                            "8 a 12 anos",
                                                            "Ensino Medio Completo",
                                                            "Ensino Tecnico",
                                                            "Superior Incompleto",
                                                            "Superior Completo"))

# CRIANDO UMA VARIAVEL CATEGORICA BINARIA DIVIDINDO AS CRIANCAS EM BAIXO PESO AO NASCER (<2500g) e NAO BAIXO PESO

bebes$baixo_peso_nasc <- ifelse(bebes$peso_nasc_g < 2500, "Baixo Peso", "Nao Baixo peso")
bebes$baixo_peso_nasc <- as.factor(bebes$baixo_peso_nasc)
summary(bebes$baixo_peso_nasc)

# CRIANDO UMA VARIAVEL CATEGORICA DIVINDO O PESO AO NASCER NAS SEGUINTES CATEGORIAS:
# <1000g, de 1000 a 1499g, de 1500 a 2499g, de 2500 a 4200g, >4200g

bebes$peso_nasc_cat <- cut(bebes$peso_nasc_g,  breaks = c(-Inf, 1000, 1500, 2500, 4200, Inf ),
                           labels = c("<1000g", "de 1000 a 1499g", "de 1500 a 2499g", "de 2500 a 4200g", ">4200g"))
summary(bebes$peso_nasc_cat)

# CRIANDO UMA VARIAVEL CATEGORICA DIVIDINGO A IDADE GESTACIONAL NAS SEGUINTES CATEGORIAS:
# <37 semanas, 37 a 42 semanas, > 42 semanas

bebes$tempo_gestacao_semanas_cat <- cut(bebes$tempo_gestacao_semanas,  breaks = c(-Inf, 37, 42, Inf ),
                                        labels = c("37 semanas", "37 a 42 semanas", "> 42 semanas"))
summary(bebes$tempo_gestacao_semanas_cat)

# RECODIFICANDO A VARIAVEL TABAGISMO MATERNO EM 3 CATEGORIAS:
# MULHERES QUE FUMAM ATUALMENTE, MULHERES QUE FUMARAM MAS PARARAM ANTES OU ATE A GRAVIDEZ ATUAL, E MULHERES QUE NUNCA FUMARAM

summary(bebes$tabagismo_mae)
bebes$tabagismo_mae_3cat[bebes$tabagismo_mae == "Fumou ate gravidez atual" | bebes$tabagismo_mae == "Fumou mas parou" ] <- "Tabagismo pregresso" 
bebes$tabagismo_mae_3cat[bebes$tabagismo_mae == "Fuma atualmente"] <- "Fuma atualmente" 
bebes$tabagismo_mae_3cat[bebes$tabagismo_mae == "Nunca fumou"] <- "Nunca fumou" 

bebes$tabagismo_mae_3cat <- as.factor(bebes$tabagismo_mae_3cat)
summary(bebes$tabagismo_mae_3cat)

#### histogramas - aula5

library(ggplot2)

fig <- ggplot(bebes,aes(peso_nasc_g))
fig
fig + geom_histogram(binwidth=50,color="white",fill="darkgreen")


fig + geom_histogram(aes(y= ..density..),color="white",fill="darkgreen")

fig + geom_histogram(color = "white") + facet_grid(. ~ tabagismo_mae_3cat ~.)
fig + geom_histogram(color = "white") + facet_wrap(. ~ tabagismo_mae_3cat)


# Removendo ausentes

!is.na(bebes$tabagismo_mae)

bebes2 <- subset(bebes, !is.na(bebes$tabagismo_mae))

fig2 <- ggplot(bebes2, aes(x = peso_nasc_g))

fig2 + geom_histogram(color = "white") + facet_grid(. ~ tabagismo_mae_3cat)

fig2 + geom_histogram(color = "white") + facet_grid(tabagismo_mae_3cat ~.)
#######################

fig2 + geom_histogram(color = "white", aes( y = ..density.. )) + 
  facet_grid(tabagismo_mae_3cat ~.)

fig2 + geom_histogram(color = "white", aes( y = ..density.. , fill = 
                                                tabagismo_mae_3cat )) + facet_grid(tabagismo_mae_3cat ~.)

fig2 + geom_histogram(color = "white", aes( y = ..density.. , fill = 
                                                tabagismo_mae_3cat )) + facet_grid(tabagismo_mae_3cat ~.) + guides(fill = F)

############################

fig2 + geom_histogram(color = "white", aes(fill=tabagismo_mae_3cat)) 
fig2 + geom_histogram(color = "white", aes(fill=tabagismo_mae_3cat), 
                        position = "identity") 
fig2 + geom_histogram(color = "white", aes(fill=tabagismo_mae_3cat), 
                        position = "identity", alpha = 0.5)

################### POLIGONOS DE FREQ

fig2 + geom_freqpoly( )

fig2 + geom_freqpoly(aes(color = tabagismo_mae_3cat))
fig2 + geom_freqpoly( ) + geom_histogram()

fig2 + geom_freqpoly(aes(color = tabagismo_mae_3cat))

fig2 + geom_freqpoly(aes(color = tabagismo_mae_3cat, y = ..density..))
fig2 + geom_freqpoly(aes(color = tabagismo_mae_3cat, y = ..density..), binwidth = 100)


fig2 + geom_density( ) 
fig2 + geom_density(aes(color = tabagismo_mae_3cat))
fig2 + geom_density(aes(color = tabagismo_mae_3cat, fill = tabagismo_mae_3cat))
fig2 + geom_density(aes(color = tabagismo_mae_3cat, fill = tabagismo_mae_3cat), 
                      alpha = 0.4)

fig2 + geom_histogram(aes(y= ..density..),color="white",fill="darkgreen") + geom_density( ) 


fig2 + geom_density(aes(color = raca_mae, fill = raca_mae), alpha = 0.2)

############### Boxplot


fig + geom_boxplot(aes(x= raca_mae, y = peso_nasc_g))

bebes3 <- subset(bebes, !is.na(raca_mae))
fig3 <- ggplot(bebes3, aes(x = peso_nasc_g))

fig3 + geom_boxplot(aes(x= raca_mae, y = peso_nasc_g))


fig3 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g), y = peso_nasc_g))
fig3 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = peso_nasc_g))

fig3 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g), notch = T)


fig3 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = educacao_mae))


bebes4 <- subset(bebes, !is.na(raca_mae) & !is.na(educacao_mae))

fig4 +
  geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                      peso_nasc_g, fill = educacao_mae))

#######PAINEIS

fig4 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = raca_mae)) + facet_grid(.~educacao_mae)

fig4 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = raca_mae)) + facet_grid(.~educacao_mae) + 
  theme(axis.text.x = element_text(angle=90, hjust = 1))


fig4 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = raca_mae)) + facet_grid(.~educacao_mae) + 
  theme(axis.text.x = element_text(angle=90, hjust = 1))

### Removendo A legenda

fig4 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = raca_mae)) + facet_grid(.~educacao_mae) + 
  theme(axis.text.x = element_text(angle=90, hjust = 1)) + guides(fill = F)

fig4 + geom_boxplot( aes(x= reorder(raca_mae, peso_nasc_g, median), y = 
                           peso_nasc_g, fill = raca_mae)) + facet_grid(.~educacao_mae) + 
  theme(legend.position = "bottom", axis.text.x = element_blank( ), axis.ticks= 
          element_blank( ))

############ BARRAS

ggplot(bebes, aes(baixo_peso_nasc)) + geom_bar( )

ggplot(bebes, aes(y = baixo_peso_nasc)) + geom_bar( )
ggplot(bebes, aes(x = baixo_peso_nasc)) + geom_bar( ) + coord_flip( )


ggplot(bebes, aes(x = baixo_peso_nasc, fill = tabagismo_mae)) + geom_bar( )
 # Usando banco sem dados missing para tabagismo_mae
ggplot(bebes2, aes(x = baixo_peso_nasc, fill = tabagismo_mae)) + geom_bar( )

ggplot(bebes2, aes(x = baixo_peso_nasc, fill = tabagismo_mae,)) + 
  geom_bar(position = "dodge")

ggplot(bebes2, aes(x = baixo_peso_nasc, fill = tabagismo_mae_3cat)) + 
  geom_bar(position = "fill")

ggplot(bebes2, aes(x = baixo_peso_nasc, fill = tabagismo_mae_3cat)) + 
  geom_bar(position = "fill") + scale_y_continuous(labels = scales::percent) + 
  ylab("Porcentagem") + xlab("") + 
  scale_fill_discrete(name = "Tabagismo materno") + theme(legend.position = "top")


#
#
# EXERCICIO
#
#

nhanes <- read.xlsx("NHANES.xlsx")

# 3. CRIE UM BANCO DE DADOS CONTENDO APENAS OS DADOS DOS INDIVIDUOS ADULTOS (IDADE MAIOR OU IGUAL A 18 ANOS) DO BANCO NHANES. 
# SUGESTAO: USAR A FUNCAO subset( )

nhanes2 <- subset(nhanes, (Age>=18))

library(gmodels)
CrossTable(nhanes$Age)
!is.na(nhanes2$Age)
nhanes3 <- subset(nhanes2, !is.na(nhanes2$Age))

fig <- ggplot(nhanes3,aes(BPSysAve))
fig + 
#  ggplot(nhanes3,aes(BPSysAve)) + geom_histogram(color = "white")
## 4

fig + geom_histogram(binwidth=50,color="white",fill="darkgreen")

## 5

fig + geom_histogram(aes(y= ..density..),color="white",fill="darkgreen")

## 6
fig + geom_histogram(aes(y= ..density..),color="white",fill="darkgreen",binwidth=2)

## 7
fig + geom_histogram(color="white",fill="darkgreen",binwidth=2) + facet_grid(Diabetes ~. )

nhanes4 <- subset(nhanes3, !is.na(nhanes3$Diabetes))
fig <- ggplot(nhanes4,aes(BPSysAve))

fig + geom_histogram(color="white",fill="darkgreen",binwidth=2) + facet_grid(Diabetes ~.)

## 8 

fig + geom_freqpoly(aes(color = Diabetes))

## 9 
fig + geom_freqpoly(aes(color = Diabetes, y = ..density..))

## 10 

fig + geom_density(aes(color = Diabetes, fill = Diabetes)) 

## 11

fig + geom_density(aes(color = Diabetes, fill = Diabetes)) + facet_grid(Diabetes ~.)

## 12

fig + geom_boxplot(aes(x= SleepTrouble , y =BPSysAve )) 

## 13

fig + geom_boxplot(aes(x= SleepTrouble , y =BPSysAve ))+ facet_grid( .~BMI_WHO)

## 14

ggplot(nhanes4, aes(x = SleepTrouble, fill = BMI_WHO,)) + 
  geom_bar(position = "dodge")

ggplot(nhanes4, aes(x = SleepTrouble, fill = BMI_WHO,)) + 
  geom_bar(position = "fill") +  facet_grid(Gender ~.)


# 16
ggplot(nhanes4,aes(y=SleepHrsNight, x = BMI_WHO)) + geom_jitter(alpha = 0.2) + stat_smooth(method = "lm")

ggplot(nhanes4 , aes( x =SleepHrsNight  , y =BMI )) + geom_point()+ stat_smooth(method = loess)+
  facet_grid( .~Gender)

fig <- ggplot(nhanes4,aes(y=SleepHrsNight, x = BMI))
fig + geom_point( ) + stat_smooth(method = "lm")+ facet_grid( .~Gender)


######################
